<!doctype html>
<html lang="en">
<meta name="csrf-token" content="{{ csrf_token() }}">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title></title>
    <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase.js"></script>
    <script src="https://www.gstatic.com/firebasejs/4.12.1/firebase-firestore.js"></script>

</head>
<body>
    <div class="container">
        <h1 align="center">Cuestionarios HONDA</h1>
       <!-- <input type="text" id="nombre" name="nombre" placeholder="nombre" class="form-control my-3">
        <input type="text" id="apellido" name="apellido" placeholder="apellido" class="form-control my-3">
        <input type="text" id="fecha" name="fecha" placeholder="fecha" class="form-control my-3">
        <button class="btn btn-info" id="boton" onclick="guardar()">Guardar</button>-->
        <button class="btn btn-info" id="btnQuenstions" onclick="tableToExcel('questions', 'Questions')">Exportar a Excel</button>
         <table id="questions" class="table my-3 table-bordered">
              <thead class="thead-dark">
                <tr>
                  <th scope="col">FECHA</th>
                  <th scope="col">DISTRIBUIDOR</th>
                  <th scope="col">CUESTIONARIO</th>
                  <th scope="col">SUPERVISOR</th>

              </tr>
          </thead>
          <tbody id="questionnaires">
          </tbody>
      </table>
  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src='/js/excel.js'></script>
  <script src='/js/app.js'></script>
</body>
</html>